const client = require('../index');

const run = async () => {
  await client.init();
  // console.log('Test: model, attribute, and action');
  // const eventData1 = {
  //   model: 'a',
  //   action: 'c',
  //   message: {
  //     test: 'hello world to a:c',
  //   },
  // };
  //
  // const result1 = await client.publishAndReceiveEvent(eventData1);
  // console.log(result1);
  const eventData2 = {
    model: 'eta',
    attribute: 'etaAndDistance',
    action: 'estimate',
    message: {
      test: 'hello world to a:b:c',
    }
  };
  const result2 = await client.publishAndReceiveEvent(eventData2);
  console.log('result', result2);
};

run();
