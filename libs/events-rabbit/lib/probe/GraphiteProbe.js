const StatsD = require('hot-shots');
const os = require('os');

const NODE_ENV = process.env.NODE_ENV;

class GraphiteProbe {
  constructor(service, rate) {
    if (NODE_ENV === 'PRODUCTION') {
      this.service = service;
      this.hostname = os.hostname();
      this.rate = rate;
      try {
        this.client = new StatsD({
          host: '172.19.2.27',
          port: 8125,
        });
      } catch (error) {
        console.error('GraphiteProbe', error);
      }
      this.client.socket.on('error', (error) => {
        console.error('GraphiteProbe', error);
      });
    }
  }

  timing(queue, start, end) {
    if (!this.service) {
      return;
    }
    const metric = `messaging.hostname.${this.hostname}.service.${this.service}.queue.${queue}.response_time`;
    this.client.timing(metric, end - start, 0.05);
  }
}

module.exports = GraphiteProbe;
