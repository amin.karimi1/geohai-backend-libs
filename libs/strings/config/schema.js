const { types } = require('config');


const possibleEnvironments = ['production', 'development', 'test', 'PRODUCTION', 'DEVELOPMENT', 'TEST'];
module.exports = {
    logger: {
        loggerName: types.String('Access Management loggerName', 'STRINGS'),
        consoleStream: types.Boolean('consoleStream', true, 'CONSOLE_STREAM'),
        consoleLogLevel: types.String('The application consoleLogLevel', 'error', 'CONSOLE_LOG_LEVEL'),
        fileStream: types.Boolean('fileStream', false, 'FILE_STREAM'),
        fileLogLevel: types.String('The application fileLogLevel', 'error'),
        rotating: types.Boolean('rotating', false),
        fileName: types.String('logger output fileName', 'test.log', 'FILE_NAME'),
    },

    env: types.Schema(possibleEnvironments, 'The application environment', 'test', 'NODE_ENV'),
};
