module.exports = {
  logger: {
    loggerName: 'STRINGS',
    consoleStream: false,
    logStashStream: false,
    logStashLogLevel: 'warn',
    fileStream: true,
    fileLogLevel: 'warn',
    rotating: true,
    rotatingPeriod: '1d',
    rotatingFilesCount: 90,
    fileName: '/home/gitlab-runner/application-logs/strings.log',
  },
};
