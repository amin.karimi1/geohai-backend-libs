
const util = require('util');
const JaegerTracing = require('./application/jaegertracing');

let tracer;

function isJaegerTracingEnable() {
  return tracer && tracer.enable;
}

function wrapAsyncMethod(method, operationName) {
  return async (...args) => {
    const span = isJaegerTracingEnable() ? tracer.createSpan(operationName) : null;
    const result = await method(...args);
    if (isJaegerTracingEnable()) span.finish();
    return result;
  };
}

function wrapSyncMethod(method, operationName) {
  return (...args) => {
    const span = isJaegerTracingEnable() ? tracer.createSpan(operationName) : null;
    const result = method(...args);
    if (isJaegerTracingEnable()) span.finish();
    return result;
  };
}

function wrapMethodsInObject(functions, prefix) {
  for (const methodName of Object.keys(functions)) {
    const method = functions[methodName];
    if (util.types.isAsyncFunction(method)) {
      functions[methodName] = wrapAsyncMethod(method, `${prefix}::${methodName}`);
    } else {
      functions[methodName] = wrapSyncMethod(method, `${prefix}::${methodName}`);
    }
  }
  return functions;
}

function createTracer(service, redis) {
  tracer = new JaegerTracing(service, redis);
  return tracer;
}

module.exports = {createTracer, wrapMethodsInObject, wrapAsyncMethod, wrapSyncMethod};
