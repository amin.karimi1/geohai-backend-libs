module.exports = function (grunt) {
  grunt.initConfig({
    jshint: {
      all: {
        options: {
          jshintrc: true,
          ignores: ['**/node_modules/**/*.js', 'server/public/**/*.js', 'coverage/**'],
          reporterOutput: '',
        },
        files: {
          src: ['**/*.js'],
        },
      },
    },
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.registerTask('default', ['jshint']);
  grunt.registerTask('test', [
    'jshint',
  ]);
};
